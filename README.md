1. Установка
```
python3 -m venv env
env/bin/pip install -r requirements.txt
```
2. Создание файла config.py
```
#!env/bin/python3

DBHOST=''
DBUSER=''
DBPASSWORD=''
DBNAME=''
#колличество дней после которого нужно удалить данные
DELETEDAY=1
#лимит записей для удаления за одну этерацию
LIMIT=100000
```