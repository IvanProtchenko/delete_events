#!env/bin/python3

from config import DBHOST, DBUSER, DBPASSWORD, DBNAME, DELETEDAY, LIMIT
import pymysql.cursors
import time

# Connect to the database
connection = pymysql.connect(host=DBHOST,
                             user=DBUSER,
                             password=DBPASSWORD,
                             db=DBNAME,
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)
unixime=int(time.time())- int(DELETEDAY)*60*60*24
try:
    with connection.cursor() as cursor:
        # Read a single record
        sql = "SELECT count(*) as 'delete' FROM %s.events WHERE clock < %s AND eventid NOT IN (SELECT eventid FROM %s.problem WHERE r_eventid is NULL) LIMIT %s;" % (DBNAME, unixime, DBNAME, LIMIT)
        cursor.execute(sql)
        result = cursor.fetchone()
        print(result)
    
    loopd = int(result['delete']/LIMIT)+1
    for d in range(int(loopd)):
        with connection.cursor() as cursor:
            # Create a new record
            sql = "DELETE FROM %s.events WHERE clock < %s AND eventid NOT IN (SELECT eventid FROM %s.problem WHERE r_eventid is NULL) LIMIT %s;" % (DBNAME, unixime, DBNAME, LIMIT)
            cursor.execute(sql)

        # connection is not autocommit by default. So you must commit to save
        # your changes.
        connection.commit()
        print('commit %s in %s' % (d,loopd))
        time.sleep(0.5)
  
finally:
    connection.close()